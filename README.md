# Tormenta20

## Descrição
Este é um sistema NÃO OFICIAL feito e mantido por fãs, sem qualquer afiliação a Tormenta20 ou a Jambo Editora.
Tormenta 20 é um RPG brasileiro e pertence a Jambo Editora.



## Colaboradores
* TheTruePortal
* Mateus Marochi
* Matheus Clemente
* Roberto Caetano
* Victor Kullack
* Alexandre Galdino
* André Oliveira
* Vinicius Lima Silva
* Gustavo Reis

## Atribuições
- Arte de alguns ícones de magias: J. W. Bjerk (eleazzaar) -- www.jwbjerk.com/art -- find this and other open art at: http://opengameart.org
- [sdenec](https://github.com/sdenec/) pelo módulo [Tidy5e Sheet](https://github.com/sdenec/tidy5e-sheet), cujo código foi adaptado neste sistema.
- [syl3r86](https://github.com/syl3r86) pelo módulo [FavTab](https://github.com/syl3r86/favtab), que foi adaptado neste sistema.
- [Tijmen Bok](https://gitlab.com/Furyspark) pelo sistema [Pathfinder 1e for Foundry VTT](https://gitlab.com/Furyspark/foundryvtt-pathfinder1), cujo código foi adaptado neste sistema.
- Este sistema usa artes de tokens do [Forgotten Adventures](https://www.forgotten-adventures.net). This system uses token arts from [Forgotten Adventures](https://www.forgotten-adventures.net).